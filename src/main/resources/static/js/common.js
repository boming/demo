//ajax请求
const baseUrl = 'http://212.64.29.11:8005/demo/';
// const baseUrl = 'http://localhost:8005/demo/';
function Ajax(method,url,postData,callback) {
    $.ajax({
        type:method,
        cache: false,
        url: baseUrl + url,
        data: postData,
        dataType:'json',
        contentType: "application/json;charset=utf-8",
        success: function(data) {
            callback(data);
        },
        error: function() {
            callback(false);
        }
    });
}
//ajax请求
function GetAjax(url,postData,callback) {
    Ajax('GET',url,postData,callback);
}
//ajax请求
function PostAjax(url,postData,callback) {
    Ajax('post',url,postData,callback);
}
function FormAjax(method,url,postData,callback) {
    $.ajax({
        type:method,
        cache: false,
        url: baseUrl + url,
        data: postData,
        dataType:'json',
        // contentType: "application/json;charset=utf-8",
        success: function(data) {
            callback(data);
        },
        error: function() {
            callback(false);
        }
    });
}
function FormGetAjax(url,postData,callback) {
    FormAjax('get',url,postData,callback);
}
function FormPostAjax(url,postData,callback) {
    FormAjax('post',url,postData,callback);
}
function FormSubmit(formId,action,method){
    $("#" + formId).attr("action",baseUrl + action).attr("method",method).submit();
}
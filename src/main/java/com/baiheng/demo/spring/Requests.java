package com.baiheng.demo.spring;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

//import cn.chinaflame.park.aop.NonWebRequestAttributes;

/**
 * 请求bean
 * @author bm
 * 2018年3月15日 下午3:19:13
 */
public class Requests {

	private static final Logger log = LoggerFactory.getLogger(Requests.class);
    public static HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = null;
        try{
            requestAttributes = RequestContextHolder.currentRequestAttributes(); 
        }catch (IllegalStateException e){
            return null;
        }
    	ServletRequestAttributes sra = (ServletRequestAttributes) requestAttributes;
    	return sra.getRequest();
    }

    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    public static HttpSession getSession(boolean isCreate) {
        return getRequest().getSession(isCreate);
    }
    
    public static boolean isAjaxRequest() {
    	return getRequest().getHeader("X-REQUESTED-WITH") == null ? false : true;
    }
    
    public static void printCookie() {
    	HttpServletRequest req = getRequest();
    	Enumeration<String> headers = req.getHeaderNames();
    	while(headers.hasMoreElements()) {
    		String name = headers.nextElement();
    		log.info("请求头name:{},value:{}", name, req.getHeader(name));
    	}
    	
    	Cookie[] cookies = getRequest().getCookies();
    	if(cookies != null)
	    	for(int i=0;i<cookies.length;i++) {
	    		log.info("cookie信息name:{},value:{}",cookies[i].getName(),cookies[i].getValue());
	    	}
    }
    
    
    public static String getSessionIdByCookie(){
    	if(getRequest() == null) return null;
    	Cookie[] cookies = getRequest().getCookies();
    	if(cookies == null || cookies.length == 0) return null;
    	for(Cookie cookie : cookies){
    		if("JSESSIONID".equalsIgnoreCase(cookie.getName()))
    			return cookie.getValue();
    		else if("SESSION".equalsIgnoreCase(cookie.getName()))
    			return cookie.getValue();
    	}
    	return null;
    }

    public static String getRequestAddr() {
    	HttpServletRequest req = getRequest();
    	String ip = req.getHeader("x-forwarded-for");
    	if(!Strings.isNullOrEmpty(ip)){
    		ip = ip.split(",")[0].trim();
    	}
    	if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    		ip = req.getHeader("Proxy-Client-IP");
    		log.info("客户端请求ip1:{}", ip);
    	}
    	if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    		ip = req.getHeader("WL-Proxy-Client-IP");
    		log.info("客户端请求ip2:{}", ip);
    	}
    	if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    		ip = req.getRemoteAddr();
    		log.info("客户端请求ip3:{}", ip);
    	}
    	return ip;
    }
}

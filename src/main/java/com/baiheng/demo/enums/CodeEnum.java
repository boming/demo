package com.baiheng.demo.enums;
/**
* @author  created by boming
* @date    2018年1月4日 上午10:09:25
**/
public enum CodeEnum {

	SUCCESS("OK","成功"),
	FAIL("ERROR","系统异常"),
	;
	
	private String code;
	private String msg;
	CodeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}

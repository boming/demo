package com.baiheng.demo.api;

import com.baiheng.demo.service.UserService;
import com.baiheng.demo.utils.Digests;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "login/", produces = {"application/json"})
@Slf4j
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    public Object login(String username, String password){
        log.info("登录接口username:{},password:{}", username, password);
        return userService.login(username, password);
    }
}

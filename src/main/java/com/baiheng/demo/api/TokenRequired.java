package com.baiheng.demo.api;

import java.lang.annotation.*;

/**
 * Created by baiming on 2015/7/26.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TokenRequired {

}

package com.baiheng.demo.api;

import com.baiheng.demo.elm.DataUtil;
import com.baiheng.demo.handler.Result;
import com.baiheng.demo.service.UserService;
import com.baiheng.demo.utils.ExcelUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "shop/")
@Slf4j
public class ShopController {

    @Autowired
    private UserService userService;

//    @TokenRequired
    @RequestMapping(value = "download", method = {RequestMethod.GET, RequestMethod.POST})
    public void login(HttpServletRequest request, HttpServletResponse response){
        log.info("下载文件");
        List<List<String>> data = DataUtil.getData();
        ExcelUtil.downloadExcel("elm_data.xlsx","data", data, response);
        userService.queryList();
//        return Result.create("123456");
    }
}

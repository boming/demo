package com.baiheng.demo.mapper;

import com.baiheng.demo.model.CpToken;
import com.baiheng.demo.model.CpTokenExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CpTokenMapper {
    int countByExample(CpTokenExample example);

    int deleteByExample(CpTokenExample example);

    int deleteByPrimaryKey(String id);

    int insert(CpToken record);

    int insertSelective(CpToken record);

    List<CpToken> selectByExample(CpTokenExample example);

    CpToken selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CpToken record, @Param("example") CpTokenExample example);

    int updateByExample(@Param("record") CpToken record, @Param("example") CpTokenExample example);

    int updateByPrimaryKeySelective(CpToken record);

    int updateByPrimaryKey(CpToken record);
}
package com.baiheng.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test")
public class TestController {
    private static Logger log = LoggerFactory.getLogger(TestController.class);

//    @Transactional
    @RequestMapping(value = "test", method = {RequestMethod.GET, RequestMethod.POST})
    public Object test(String param){
        log.info("入参param:{}", param);
        return null;
    }
}

package com.baiheng.demo.handler;

import com.baiheng.demo.enums.CodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 捕获controller异常类
 * @author created by lbq
 *
 */
@ControllerAdvice(value = "com.baiheng.demo.api")
public class ExceptionHandle {
	private final static Logger logger=LoggerFactory.getLogger(ExceptionHandle.class);

	/**
	 * 
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value=Exception.class)
	@ResponseBody
	public Object handle(Exception e) {
		if(e instanceof HandlerException) {
			HandlerException he = (HandlerException) e;
			logger.error("业务异常{}",e);
			return Result.create(he.getCode(), he.getMsg());
		}else {
			System.err.println(e);
			logger.error("系统异常{}",e);
			return Result.create(CodeEnum.FAIL.getCode(), CodeEnum.FAIL.getMsg());
		}
	}
}

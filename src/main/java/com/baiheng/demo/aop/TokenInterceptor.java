package com.baiheng.demo.aop;

import com.baiheng.demo.api.TokenRequired;
import com.baiheng.demo.enums.CodeEnum;
import com.baiheng.demo.handler.HandlerException;
import com.baiheng.demo.service.CpTokenService;
import com.baiheng.demo.spring.Requests;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
@Slf4j
public class TokenInterceptor implements WebHandleInterceptor {

	@Autowired
	private CpTokenService cpTokenService;

	@Override
	public int getOrder() {
		return 0;
	}

	@Override
	public void preHandle(MethodSignature methodSignature){
		log.info("权限拦截");
		Method method = methodSignature.getMethod();
		TokenRequired tokenRequired = method.getAnnotation(TokenRequired.class);
		if(tokenRequired != null){
			String userId = Requests.getRequest().getParameter("userId");
			String token = Requests.getRequest().getParameter("token");
			boolean isPass = cpTokenService.validToken(userId, token);
			if(isPass) return;
			throw new HandlerException(CodeEnum.FAIL.getCode(), "token校验失败");
		}
	}
}

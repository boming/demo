package com.baiheng.demo.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.OrderComparator;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by json on 2015/7/11.
 */
@Aspect
@Component
@Slf4j
public class WebHandleInterceptorAdvisor implements ApplicationContextAware {

    private ApplicationContext context;
    public static final String ERROR_PAGE = "error_page";
    
    private  static  final Logger logger = LoggerFactory.getLogger(WebHandleInterceptorAdvisor.class);  
    
    private WebHandleInterceptor[] interceptors;

    @Around("execution(* com.baiheng.demo.api..*Controller.*(..))")
    public Object aroundHandle(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("joinPoint: {}", joinPoint);
        long start = System.currentTimeMillis();
        log.info("*******接口开始method:{},start:{}", joinPoint, start);
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        if (interceptors != null) {
            for (WebHandleInterceptor wmi : interceptors) {
            	wmi.preHandle(ms);
            }
        }
        Object o = joinPoint.proceed();
        long end = System.currentTimeMillis();
        log.info("*******接口返回method:{},end:{},end-start:{}", joinPoint, end, (end-start));
        return o;
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
    	this.context = context;
    	Map<?, WebHandleInterceptor> vr = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, WebHandleInterceptor.class, true, false);
        log.info("Interceptors: {}", vr);
        if (!vr.isEmpty()) {
            this.interceptors = new WebHandleInterceptor[vr.size()];
            vr.values().toArray(this.interceptors);
            OrderComparator.sort(this.interceptors);
        }
    }
    
   
      
    
}

package com.baiheng.demo.test;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class TestMain {
    public static void main(String[] args) {
//        Map<String, String> provinceMap = Maps.newHashMap();
//        Map<String, String> cityMap = Maps.newHashMap();
//        Map<String, String> areaMap = Maps.newHashMap();
//        parse(provinceMap, cityMap, areaMap);
//        System.out.println(provinceMap.get("北京市"));
//        System.out.println(cityMap.get("北京市北京市"));
        Double d = (Math.random() * 9 + 1) * 1000;
        System.out.println(d);
        System.out.println(d.intValue());
    }
    private static void parse(Map<String, String> provinceMap, Map<String, String> cityMap, Map<String, String> areaMap){
        String path = TestMain.class.getResource("/").getPath();
        File file = new File(path + "area.json");
        StringBuilder sb = new StringBuilder();
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
            while(scanner.hasNextLine()) {
                sb.append(scanner.nextLine().replaceAll("\\s*", ""));
            }
            JsonParser parser = new JsonParser();
            JsonElement root = parser.parse(sb.toString());
            JsonArray ary = root.getAsJsonArray();
            for(int i=0;i<ary.size();i++){
                JsonObject obj = ary.get(i).getAsJsonObject();
                String provinceCode = obj.getAsJsonPrimitive("code").getAsString();
                String provinceName = obj.getAsJsonPrimitive("name").getAsString();
                provinceMap.put(provinceName,provinceCode);
                JsonArray cityAry = obj.getAsJsonArray("children");
                if(cityAry != null){
                    for(int j=0;j<cityAry.size();j++){
                        JsonObject cityObj = cityAry.get(j).getAsJsonObject();
                        String cityCode = cityObj.getAsJsonPrimitive("code").getAsString();
                        String cityName = cityObj.getAsJsonPrimitive("name").getAsString();
                        cityMap.put(provinceName+cityName,cityCode);
                        JsonArray areaAry = cityObj.getAsJsonArray("children");
                        if(areaAry != null){
                            for(int k=0;k<areaAry.size();k++){
                                JsonObject areaObj = areaAry.get(k).getAsJsonObject();
                                String areaCode = areaObj.getAsJsonPrimitive("code").getAsString();
                                String areaName = areaObj.getAsJsonPrimitive("name").getAsString();
                                String key = provinceName + cityName + areaName;
                                areaMap.put(key, areaCode);
                            }
                        }
                    }
                }
            }
            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.baiheng.demo.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelUtil {

	/**
	 * 根据List导出xls表格
	 * 
	 * @param title		表格名
	 * @param header	表格标题
	 * @param list		表格数据
	 * @param headExtra	头部附加数据
	 * @param footExtra	尾部附加数据
	 * @param operator	操作员
	 * @return 			表格文件下载地址
	 * @throws Exception
	 */
	public static void downList(String title, List header, List list, Map headExtra, Map footExtra, String operator,HttpServletResponse resp) throws Exception {

		String nowtime = DateTimes.nowTimestamp(DateTimes.TIMESTAMP_PATTERN3);
		String filename = "REPORT_"+ nowtime +"_"+ (int) (Math.random() * 10000) + ".xls";
		
//		resp.setContentType("application/msexcel;charset=UTF-8");
//		resp.setContentType("multipart/form-data;charset=UTF-8");
		resp.setHeader("Content-disposition", "attachment;filename="+filename);
		OutputStream os = resp.getOutputStream();
//		FileOutputStream os = new FileOutputStream("E://uploadReport/"+title+".xls");
		
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet(title);
		Row rowHeader = sheet.createRow(0);
		
		int filerow=0;
		// 写入标题（报表名）
		//CellRangeAddress（开始行数，结束行数，开始列数，结束列数）0到1是两行
		int size = 1;
		if(header!=null){
			size = header.size();
		}
		CellRangeAddress cra=new CellRangeAddress(0, 1, 0, size-1);
		sheet.addMergedRegion(cra);
		Cell cell = rowHeader.createCell(0);
        cell.setCellValue(title);
        rowHeader = sheet.createRow(2);
		//标题占两行，之后的行数从2开始
        filerow=2;

		// 附加头部信息，开始结束时间，占一行
		if (headExtra != null) {
			int filecol =0;//列数
			for (Iterator iter = headExtra.entrySet().iterator(); iter.hasNext();) {
				Entry entry = (Entry) iter.next();
				if(filecol % 2==0){
					Cell cell1 = rowHeader.createCell(filecol);
			        cell1.setCellValue(entry.getKey().toString());
				}
				filecol+=1;
				Cell cell2 = rowHeader.createCell(filecol);
		        cell2.setCellValue(entry.getValue().toString());
				filecol+=1;
			}
			filerow += 1;
		}

		// 查询列表号,list存入中文
		if(header!=null){
			Row headerRow = sheet.createRow(filerow);
			int filecol =0;
			for (int h = 0; h < header.size(); h++) {
				Cell cellheader = headerRow.createCell(filecol);
		        cellheader.setCellValue(header.get(h).toString());
		        filecol++;
			}
			filerow += 1;
		}
		
		// 数据信息
		if(list!=null){
			for (int i = 0; i < list.size(); i++) {
				Row listRow = sheet.createRow(filerow+i);
				for (int j = 0; j < header.size(); j++) {
					String str = (String) (((HashMap) list.get(i)).values().toArray())[j];
					Cell cellList = listRow.createCell(j);
					cellList.setCellValue(str);
				}
			}
			filerow += list.size();
		}
		
		// 附加尾部信息
		if (footExtra != null) {
			Row row = sheet.createRow(filerow);
			for (Iterator iter = footExtra.entrySet().iterator(); iter.hasNext();) {
				Entry entry = (Entry) iter.next();
				Cell cellfootExtraKey= row.createCell(0);
				cellfootExtraKey.setCellValue(entry.getKey().toString());
				Cell cellfootExtraValue= row.createCell(1);
				cellfootExtraValue.setCellValue(entry.getValue().toString());
			}
			filerow += 4;
		}

		// 附加打印信息
		Row row = sheet.createRow(filerow);
		Cell celloperatordate1= row.createCell(0);
		celloperatordate1.setCellValue("打印日期");
		String operatortime = DateTimes.format(new Date(), "MM-dd HH:mm");
		Cell celloperatordate2= row.createCell(1);
		celloperatordate2.setCellValue(operatortime);
		if (operator != null) {
			Cell celloperatorman1= row.createCell(2);
			celloperatorman1.setCellValue("打印人");
			Cell celloperatorman2= row.createCell(3);
			celloperatorman2.setCellValue(operator);
		}
		
		//resp.getWriter().print("成功");
		workbook.write(os);
		os.flush();
		os.close();
		workbook.close();
	}

	/**
	 * 保存数据到excel文件
	 * @param title		标题
	 * @param filePath	文件
	 * @param data		数据
	 */
	public static void saveExcel(String title, String filePath, List<List<String>> data){
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(title);
		FileOutputStream os = null;
		try{
			for(int i=0;i<data.size();i++){
				Row row = sheet.createRow(i);
				for(int j=0;j<data.get(i).size();j++){
					Cell cell = row.createCell(j);
					cell.setCellValue(data.get(i).get(j));
				}
			}
			os = new FileOutputStream(filePath);
			workbook.write(os);
			os.flush();
			os.close();
		}catch(IOException e){
			e.printStackTrace();
		}

	}

	/**
	 * 下载文件
	 * @param filename
	 * @param title
	 * @param data
	 * @param response
	 */
	public static void downloadExcel(String filename, String title, List<List<String>> data, HttpServletResponse response){
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(title);
		OutputStream os = null;
		response.setHeader("Content-disposition", "attachment;filename="+filename);
		response.setContentType("application/octet-stream");
		try{
			for(int i=0;i<data.size();i++){
				Row row = sheet.createRow(i);
				for(int j=0;j<data.get(i).size();j++){
					Cell cell = row.createCell(j);
					cell.setCellValue(data.get(i).get(j));
				}
			}
			os = response.getOutputStream();
			workbook.write(os);
			os.flush();
			os.close();
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}

package com.baiheng.demo.model;

import java.util.Date;

public class Shop {
    private Integer id;

    private String name;

    private String rating;

    private Integer recentOrderNum;

    private Integer orderLeadTime;

    private String address;

    private String phone;

    private String latitude;

    private String longitude;

    private String openingHours;

    private Date created;

    private Date updated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating == null ? null : rating.trim();
    }

    public Integer getRecentOrderNum() {
        return recentOrderNum;
    }

    public void setRecentOrderNum(Integer recentOrderNum) {
        this.recentOrderNum = recentOrderNum;
    }

    public Integer getOrderLeadTime() {
        return orderLeadTime;
    }

    public void setOrderLeadTime(Integer orderLeadTime) {
        this.orderLeadTime = orderLeadTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours == null ? null : openingHours.trim();
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
package com.baiheng.demo.model;

import java.io.Serializable;
import java.util.Date;

/**
 * goods
 * @author 
 */
public class Goods implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 图片地址
     */
    private String imgUrl;

    /**
     * 价格
     */
    private Long price;

    /**
     * 日售
     */
    private Integer daySale;

    /**
     * 月售
     */
    private Integer monthSale;

    /**
     * 是否上架0-上架；1-下架
     */
    private Byte shelfStat;

    /**
     * 是否删除0-正常；1-删除
     */
    private Byte state;

    /**
     * 最后更新时间
     */
    private Date updatetime;

    /**
     * 创建时间
     */
    private Date createtime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getDaySale() {
        return daySale;
    }

    public void setDaySale(Integer daySale) {
        this.daySale = daySale;
    }

    public Integer getMonthSale() {
        return monthSale;
    }

    public void setMonthSale(Integer monthSale) {
        this.monthSale = monthSale;
    }

    public Byte getShelfStat() {
        return shelfStat;
    }

    public void setShelfStat(Byte shelfStat) {
        this.shelfStat = shelfStat;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Goods other = (Goods) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getImgUrl() == null ? other.getImgUrl() == null : this.getImgUrl().equals(other.getImgUrl()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getDaySale() == null ? other.getDaySale() == null : this.getDaySale().equals(other.getDaySale()))
            && (this.getMonthSale() == null ? other.getMonthSale() == null : this.getMonthSale().equals(other.getMonthSale()))
            && (this.getShelfStat() == null ? other.getShelfStat() == null : this.getShelfStat().equals(other.getShelfStat()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getUpdatetime() == null ? other.getUpdatetime() == null : this.getUpdatetime().equals(other.getUpdatetime()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getImgUrl() == null) ? 0 : getImgUrl().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getDaySale() == null) ? 0 : getDaySale().hashCode());
        result = prime * result + ((getMonthSale() == null) ? 0 : getMonthSale().hashCode());
        result = prime * result + ((getShelfStat() == null) ? 0 : getShelfStat().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getUpdatetime() == null) ? 0 : getUpdatetime().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", imgUrl=").append(imgUrl);
        sb.append(", price=").append(price);
        sb.append(", daySale=").append(daySale);
        sb.append(", monthSale=").append(monthSale);
        sb.append(", shelfStat=").append(shelfStat);
        sb.append(", state=").append(state);
        sb.append(", updatetime=").append(updatetime);
        sb.append(", createtime=").append(createtime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
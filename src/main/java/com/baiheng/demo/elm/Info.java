package com.baiheng.demo.elm;

/**
 * @author Administrator
 * @version 2018-11-14
 */
public class Info {
    private String name;	//分类名称
    private Food[] foods;	//菜品
    private long id;	//分类ID

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Food[] getFoods() {
        return foods;
    }

    public void setFoods(Food[] foods) {
        this.foods = foods;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

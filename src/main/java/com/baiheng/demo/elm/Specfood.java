package com.baiheng.demo.elm;

/**
 * @author Administrator
 * @version 2018-11-14
 */
public class Specfood {
    private String name;	//菜品名
    private Integer restaurant_id;	//所属餐厅ID
    private Integer food_id;	//菜品ID
    private Float recent_rating;	//最近评价
    private Float price;	//价格
    private Float recent_popularity;	//最近销售量

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(Integer restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public Integer getFood_id() {
        return food_id;
    }

    public void setFood_id(Integer food_id) {
        this.food_id = food_id;
    }

    public Float getRecent_rating() {
        return recent_rating;
    }

    public void setRecent_rating(Float recent_rating) {
        this.recent_rating = recent_rating;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getRecent_popularity() {
        return recent_popularity;
    }

    public void setRecent_popularity(Float recent_popularity) {
        this.recent_popularity = recent_popularity;
    }
}

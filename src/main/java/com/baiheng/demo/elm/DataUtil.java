package com.baiheng.demo.elm;

import com.baiheng.demo.handler.HandlerException;
import com.baiheng.demo.utils.ExcelUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import com.google.gson.reflect.TypeToken;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * @author Administrator
 * @version 2018-11-14
 */
public class DataUtil {
    public static final String USER_AGENT = "<PUT YOUR USER AGENT HERE>";
    public static final String IP = "58.209.253.180";
    public static final int PORT = 45518;
    public static void main(String[] args) throws Exception {

    }
    public static List<List<String>> getData() {
        System.setProperty("http.proxyHost", IP);
        System.setProperty("http.proxyPort", "45518");
        System.setProperty("http.nonProxyHosts", "192.168.3.249 | 192.168.3.100");

        List<List<String>> data = Lists.newArrayList();
        try{
            for (int i = 0; i < 27; i++) {

                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(IP, PORT));
                String href = "https://www.ele.me/restapi/shopping/restaurants?extras%5B%5D=activities&geohash=wx4erybdnx4&latitude=39.98375&limit=24&longitude=116.35717"
                        + "&offset=" + i * 24 + "&terminal=web";
                URL url = new URL(href);
                HttpsURLConnection urlcon = (HttpsURLConnection)url.openConnection(proxy);
                urlcon.connect();         //获取连接
                Scanner scanner = new Scanner(urlcon.getInputStream());
                StringBuffer sb = new StringBuffer();
                while(scanner.hasNextLine()){
                    sb.append(scanner.nextLine());
                }
                String str = sb.toString();
                if(Strings.isNullOrEmpty(str))
                    continue;


//                String url = "https://www.ele.me/restapi/shopping/restaurants?extras%5B%5D=activities&geohash=wx4erybdnx4&latitude=39.98375&limit=24&longitude=116.35717"
//                        + "&offset=" + i * 24 + "&terminal=web";
//                Connection con = Jsoup.connect(url).ignoreContentType(true).timeout(30000).followRedirects(true)
//                        .maxBodySize(1024*1024*3).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36");
//                con.userAgent(USER_AGENT);
//                Connection.Response response = con.execute();
//                String str = response.body();
                System.out.println(str);
                System.out.println(i);
                Gson gson = new Gson();
                List<Shop> shops = gson.fromJson(str, new TypeToken<List<Shop>>() {
                }.getType());
                List<String> header = Arrays.asList("ID","商户名称","商户地址","商户电话","商户经度","商户纬度","营业时间");
                data.add(header);
                for(int j=0;j<shops.size();j++){
                    data.add(Arrays.asList(String.valueOf(shops.get(j).getId()),shops.get(j).getName(),
                            shops.get(j).getAddress(),shops.get(j).getPhone(),String.valueOf(shops.get(j).getLongitude()),
                            String.valueOf(shops.get(j).getLatitude()),String.join(",",shops.get(j).getOpening_hours())));
                }
                Thread.sleep(2 * 1000);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return data;
    }

    public static void parseFoods(List<Shop> shops) throws Exception {

        FileWriter fw = new FileWriter("E:\\elm_data.txt", true);
        BufferedWriter bw = new BufferedWriter(fw);
        System.out.println("wenjian");

        List<List<String>> data = Lists.newArrayList();
        List<String> header = Arrays.asList("ID","商户名称","商户地址","商户电话","商户经度","商户纬度","营业时间");
        data.add(header);
        for(int i=0;i<shops.size();i++){
            data.add(Arrays.asList(String.valueOf(shops.get(i).getId()),shops.get(i).getName(),
                    shops.get(i).getAddress(),shops.get(i).getPhone(),String.valueOf(shops.get(i).getLongitude()),
                    String.valueOf(shops.get(i).getLatitude()),String.join(",",shops.get(i).getOpening_hours())));
            ExcelUtil.saveExcel("饿了么商家数据","E:\\elm_data.xlsx", data);
        }
//        for (Shop shop : shops) {
//
//            String data = shop.getId() + "\t" + shop.getName() + "\t" +
//            if (food_id > 0 && name != null && restaurant_id != 0 && price != 0.0) {
//                String data = food_id + "\t" + name + "\t" + restaurant_id + "\t" + shop.getName() + "\t"
//                        + price + "\t" + recent_rating + "\t" + recent_popularity + "\r\n";
//                bw.append(data);
//                bw.flush();
//            }
//
//            String url = "https://www.ele.me/restapi/shopping/v2/menu?restaurant_id=" + shop.getId();
//            System.out.println(url);
//            Connection con = Jsoup.connect(url).ignoreContentType(true);
//            Connection.Response response = con.execute();
//
//            String str = response.body();
//            Gson gson = new GsonBuilder().create();
//
//            if (shop.getId() == 271908 || shop.getId() == 156329101 || shop.getId() == 156277299
//                    || shop.getId() == 682323 || shop.getId() == 157034079 || shop.getId() == 154898695
//                    || shop.getId() == 1001894 || shop.getId() == 2142009 || shop.getId() == 476592
//                    || shop.getId() == 305155 || shop.getId() == 156447071) {
//                break;
//            }
//
//            List<Info> infos = null;
//            try {
//                infos = gson.fromJson(str, new TypeToken<List<Info>>() {
//                }.getType());
//            } catch (Exception e) {
//                throw new RuntimeException(shop.getId() + "网页有问题！！！", e);
//            }
//
//            for (Info info : infos) {
//                Food[] foods = info.getFoods();
//                for (Food food : foods) {
//                    Specfood[] specfoods = food.getSpecfoods();
//                    for (Specfood specfood : specfoods) {
//                        Integer food_id = specfood.getFood_id();
//                        String name = specfood.getName();
//                        Integer restaurant_id = specfood.getRestaurant_id();
//                        Float price = specfood.getPrice();
//                        Float recent_rating = specfood.getRecent_rating();
//                        Float recent_popularity = specfood.getRecent_popularity();
//                        if (food_id > 0 && name != null && restaurant_id != 0 && price != 0.0) {
//                            String data = food_id + "\t" + name + "\t" + restaurant_id + "\t" + shop.getName() + "\t"
//                                    + price + "\t" + recent_rating + "\t" + recent_popularity + "\r\n";
//                            bw.append(data);
//                            bw.flush();
//                        }
//                    }
//                }
//            }
//        }
        bw.close();
    }
}

package com.baiheng.demo.elm;

import java.util.List;

/**
 * @author Administrator
 * @version 2018-11-14
 */
public class Shop {
    private String id;	//店铺ID
    private String name;	//店铺名称
    private Float rating;	//店铺评分
    private Integer recent_order_num;	//店铺总售量
    private Integer order_lead_time;	//最迟送到时间
    private String address;
    private String phone;
    private Double latitude;
    private Double longitude;
    private List<String> opening_hours;

    public List<String> getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(List<String> opening_hours) {
        this.opening_hours = opening_hours;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getRecent_order_num() {
        return recent_order_num;
    }

    public void setRecent_order_num(Integer recent_order_num) {
        this.recent_order_num = recent_order_num;
    }

    public Integer getOrder_lead_time() {
        return order_lead_time;
    }

    public void setOrder_lead_time(Integer order_lead_time) {
        this.order_lead_time = order_lead_time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}

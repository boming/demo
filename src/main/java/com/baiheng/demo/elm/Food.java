package com.baiheng.demo.elm;

/**
 * @author Administrator
 * @version 2018-11-14
 */
public class Food {
    private Float rating;	//菜品等级
    private long restaurant_id;	//所属餐厅ID
    private String description;	//描述
    private Integer month_sales;	//月售量
    private Integer rating_count;
    private String image_path;	//图片路径
    private String name;	//菜品名称
    private Integer satisfy_count;	//满意的顾客数
    private Integer satisfy_rate;	//满意度
    private Specfood[] specfoods;	//菜品详情

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public long getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(long restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMonth_sales() {
        return month_sales;
    }

    public void setMonth_sales(Integer month_sales) {
        this.month_sales = month_sales;
    }

    public Integer getRating_count() {
        return rating_count;
    }

    public void setRating_count(Integer rating_count) {
        this.rating_count = rating_count;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSatisfy_count() {
        return satisfy_count;
    }

    public void setSatisfy_count(Integer satisfy_count) {
        this.satisfy_count = satisfy_count;
    }

    public Integer getSatisfy_rate() {
        return satisfy_rate;
    }

    public void setSatisfy_rate(Integer satisfy_rate) {
        this.satisfy_rate = satisfy_rate;
    }

    public Specfood[] getSpecfoods() {
        return specfoods;
    }

    public void setSpecfoods(Specfood[] specfoods) {
        this.specfoods = specfoods;
    }
}

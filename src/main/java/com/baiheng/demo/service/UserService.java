package com.baiheng.demo.service;

import com.baiheng.demo.enums.CodeEnum;
import com.baiheng.demo.handler.HandlerException;
import com.baiheng.demo.handler.Result;
import com.baiheng.demo.mapper.UserMapper;
import com.baiheng.demo.model.User;
import com.baiheng.demo.model.UserExample;
import com.baiheng.demo.utils.Digests;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Administrator
 * @version 2018-11-15
 */
@Service
@Slf4j
public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CpTokenService cpTokenService;

    public List<User> queryList(){
        UserExample userExample = new UserExample();
        return userMapper.selectByExample(userExample);
    }

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    public Result<Map<String, String>> login(String username, String password){
        log.info("登录接口[Service]username:{},password:{}", username, password);
        if(Strings.isNullOrEmpty(username)) throw new HandlerException(CodeEnum.FAIL.getCode(),"用户名不能为空");
        if(Strings.isNullOrEmpty(password)) throw new HandlerException(CodeEnum.FAIL.getCode(),"密码不能为空");
        String md5Pwd = Digests.md5DigestAsHex(password);
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(md5Pwd);
        List<User> users = userMapper.selectByExample(userExample);
        if(users == null || users.size() == 0) throw new HandlerException(CodeEnum.FAIL.getCode(),"用户不存在");
        if(users.size() > 1) throw new HandlerException(CodeEnum.FAIL.getCode(),"用户异常");
        String key = System.currentTimeMillis() + "-" + username;
        String token = cpTokenService.createToken(key);
        Map<String, String> result = Maps.newHashMap();
        result.put("userId", key);
        result.put("token", token);
        return Result.create(result);
    }
}

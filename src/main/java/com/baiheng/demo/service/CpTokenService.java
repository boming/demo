package com.baiheng.demo.service;

import com.baiheng.demo.mapper.CpTokenMapper;
import com.baiheng.demo.model.CpToken;
import com.baiheng.demo.utils.DateTimes;
import com.baiheng.demo.utils.Digests;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

@Service
@Slf4j
public class CpTokenService {

    @Autowired
    private CpTokenMapper cpTokenMapper;
    private static final String PRE_FIX = "token:";
    private static final long EXPIRED_HOURS = 12;

    /**
     * 创建token
     * @param id
     * @return
     */
    public String createToken(String id){
        CpToken cpToken = new CpToken();
        cpToken.setId(id);
        String value = Digests.md5DigestAsHex(PRE_FIX + id);
        cpToken.setToken(value);
        String expired = DateTimes.addHourToStringDate(DateTimes.TIMESTAMP_PATTERN4, DateTimes.nowDateTime(), EXPIRED_HOURS);
        cpToken.setExpiredTime(expired);
        cpToken.setCreated(new Date());
        cpToken.setUpdated(new Date());
        cpTokenMapper.insert(cpToken);
        return value;
    }
    public boolean validToken(String id, String token){
        CpToken cpToken = cpTokenMapper.selectByPrimaryKey(id);
        if(cpToken == null) return false;
        if(Objects.equals(cpToken.getToken(),token)){
            if(!Strings.isNullOrEmpty(cpToken.getExpiredTime())){
                return DateTimes.nowDateTime().compareTo(cpToken.getExpiredTime()) > 0 ? false : true;
            }
            return true;
        }else{
            return false;
        }
    }

}

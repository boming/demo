package com.baiheng.demo.generator;
/**
 * 
 * @author  created by boming
 * @date    2018年3月6日 上午9:31:01
 */
public class ModelGeneratorMain {

	public static void main(String[] args) {
		ModelGeneratorUtil.generateTable("pa_notice");  //table generator
		ModelViewGeneratorUtil.generateTable("pa_notice");  //view generator
		ModelTestDataGenerator.generateDataByTable("pa_notice");  //table test data generator
	}
}
